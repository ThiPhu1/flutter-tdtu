import 'package:flutter/cupertino.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  static const route = '/login';

  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Image(image: AssetImage('assets/images/logo.png')),
        actions: [
          IconButton(
            icon: Icon(Icons.notifications_outlined),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.shopping_cart_outlined),
            onPressed: () {},
          )
        ],
      ),
      body: Column(
        children: [
          // slider(),
          // goodDeal(),
          // utility(),
          navBar(),
        ],
      ),
    ));
  }
}

navBar() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.end,
    children: [
      Text("Howdy")
    ],
    );
}

slider() {
  var items = [1,2,3,4];
}

goodDeal() {}

utility() {
  // return
}
