import 'package:flutter/material.dart';
import './home.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return (MaterialApp(
      routes: {
        '/': (context) => Home(),
      },
      initialRoute: '/',
    ));
  }
}
