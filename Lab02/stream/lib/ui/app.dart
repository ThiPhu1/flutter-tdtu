import 'package:flutter/material.dart';
import '../stream/word_stream.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StreamState();
  }
}

class StreamState extends State<App> {
  int count = 0;
  WordStream wordStream = WordStream();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Stream English Word",
      home: Scaffold(
        appBar: AppBar(title: Text('Stream ')),
        body: Text('You have received $count words'),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add_circle_outline_outlined),
          onPressed: () {
            receiveWord();
          },
        ),
      ),
    );
  }

  receiveWord() async {
    wordStream.getWords().listen((eventWord) {
      setState(() {
        print(eventWord);
        count += 1;
      });
    });
  }
}
