import 'package:flutter/material.dart';

class WordStream {
  Stream<String> getWords() async* {
    final List<String> words = [
      "pest",
      "shadow",
      "philosophy",
      "top",
      "integrated",
      "sock",
      "opinion",
      "encourage",
      "residence",
      "skate",
      "admit",
      "variety",
      "grass",
      "denial",
      "copy",
      "seasonal",
      "biscuit",
      "dip",
      "exploit",
      "hostage",
    ];

    yield* Stream.periodic(Duration(seconds: 1), (int t) {
      int index = t % 5;
      return words[index];
    });
  }
}
