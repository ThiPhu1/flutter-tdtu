import 'package:flutter/material.dart';
import 'package:stream_1/stream/message_stream.dart';

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return StreamState();
  }
}

class StreamState extends State<MyApp> {
  MessageStream messageStream = MessageStream();
  var messageBasket = [];

  @override
  void initState() {
    receiveMessage();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(title: Text("Message Stream")),
          body: ListView.builder(
            padding: const EdgeInsets.all(16),
            itemCount: messageBasket.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                title: Text(messageBasket[index]),
              );
            },
          )),
    );
  }

  void receiveMessage() async {
    messageStream.getMessage().listen(
      (eventMessage) {
        setState(() {
          print(eventMessage);
          if(eventMessage != null){
            messageBasket.add(eventMessage);
          }
        });
      }, cancelOnError: true
    );
  }
}
