import 'package:flutter/material.dart';
import 'dart:async';

class MessageStream {
  Stream<String> getMessage() async* {
    List<String> messages = [
      "You are so beautiful",
      "JFK",
      "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
      "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
      "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
      "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
      "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
      "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
      "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
      "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...",
    ];

    yield* Stream.periodic(Duration(seconds: 1), (int t) {
      print("time $t");
      if (messages[t] == null) {
        throw Exception('Array out of bound');
      } else
      return messages[t];
    });
  }

  // cancel(){

  // }
}
