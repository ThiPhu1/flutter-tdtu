import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/image_model.dart';

class ImageStream1 {
  Stream<dynamic> getImage() async* {
    var imagesHolder = await fetchImage();
    print(imagesHolder);

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      print("time $t");
      if (imagesHolder[t] == null) {
        throw Exception('Array out of bound');
      } else
        return imagesHolder[t];
    });
  }

  // cancel(){

  // }
}

fetchImage() async {
  var imagesHolder = [];

  var url = Uri.https('jsonplaceholder.typicode.com', 'photos');
  var response = await http.get(url);
  // print(response.body);

  var jsonObjects = json.decode(response.body);
  for (var jsonObject in jsonObjects) {
    var image =
        ImageModel(jsonObject['id'], jsonObject['title'], jsonObject['url']);

    imagesHolder.add(image);
  }
  return imagesHolder;
}
