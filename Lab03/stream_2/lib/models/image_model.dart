class ImageModel {
  int id;
  String title;
  String url;

  ImageModel(this.id, this.title, this.url);

  String toString() {
    return '($id,$title,$url)';
  }
}
