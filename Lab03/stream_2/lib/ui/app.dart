import 'package:flutter/material.dart';
import 'package:stream_1/stream/image_stream.dart';

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return StreamState();
  }
}

class StreamState extends State<MyApp> {
  ImageStream1 imageStream = ImageStream1();
  var imageBasket = [];

  @override
  void initState() {
    receiveMessage();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(title: Text("Image Stream")),
          body: ListView.builder(
            padding: const EdgeInsets.all(16),
            itemCount: imageBasket.length,
            itemBuilder: (BuildContext context, int index) {
              return Card(
                child: Column(children: [
                  Image.network(
                  imageBasket[index].url,
                  fit: BoxFit.fill
                ),
                Text(imageBasket[index].title)
                ],),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
              );
            },
          )),
    );
  }

  void receiveMessage() async {
    imageStream.getImage().listen((event) {
      setState(() {
        if (event != null) {
          imageBasket.add(event);
        }
      });
    }, cancelOnError: true);
  }
}
