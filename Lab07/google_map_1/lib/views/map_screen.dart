import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  late GoogleMapController controller;
  Location location = Location();

  void onMapCreated(GoogleMapController ctrlr) {
    controller = ctrlr;
    location.onLocationChanged.listen((event) {
      controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(
            target: LatLng(event.latitude!, event.longitude!), zoom: 15),
      ));
    });
  }

  void handleTap(LatLng position) {
    setState(() {
      myMarkers.add(Marker(
        markerId: MarkerId(position.toString()),
        position: position,
      ));
    });
  }

  List<Marker> myMarkers = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        // appBar: AppBar(
        //   title: const Text("My Map"),
        // ),
        body: mapContainer(),
      ),
    );
  }

  mapContainer() {
    return GoogleMap(
      initialCameraPosition: _kGooglePlex,
      onMapCreated: onMapCreated,
      myLocationEnabled: true,
      markers: Set.from(myMarkers),
      onTap: handleTap,
    );
  }
}
