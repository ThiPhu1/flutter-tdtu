library string_util;

class StringUtil {
  int wordCount(String string) => string.trim().split(' ').length;
}
