import 'package:flutter/material.dart';
import 'package:date_time_picker/date_time_picker.dart';
import '../bloc/bloc_stream.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(title: Text("My login form")),
      body: LoginForm(),
    ));
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<LoginForm> {
  // Define global key for form
  final _formKey = GlobalKey<FormState>();
  late String email;
  late String password;

  final bloc = Bloc();

  String errorMsg = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            emailField(),
            lastNameField(),
            firstNameField(),
            dobField(),
            addressField(),
            loginButton()
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: TextFormField(
          decoration: const InputDecoration(
            icon: Icon(Icons.person),
            labelText: "Email Address",
          ),
          keyboardType: TextInputType.emailAddress,
          // validator: (value) {
          //   if (value == null || value.isEmpty) {
          //     return 'Please provide an email address';
          //   } else if (!value.contains("@")) {
          //     return 'Email not valid';
          //   }
          //   return null;
          // },
          onSaved: (value) {
            email = value as String;
          },
          onChanged: (String value) {
            print("On changed $value");

            bloc.emailStreamController.sink.add(value);

            if (!value.contains('@')) {
              errorMsg = 'Invalid value';
            }

            setState(() {
              print('Update state!');
              print('$errorMsg');
            });
          },
        ));
  }

  Widget lastNameField() {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: TextFormField(
          decoration: const InputDecoration(
            icon: Icon(Icons.person),
            labelText: "Lastname",
          ),
          keyboardType: TextInputType.emailAddress,
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Please enter your lastname';
            }
            return null;
          },
        ));
  }

  Widget firstNameField() {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: TextFormField(
            decoration: const InputDecoration(
              icon: Icon(Icons.person),
              labelText: "Firstname",
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter your firstname';
              }
              return null;
            }));
  }

  Widget dobField() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 16),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: DateTimePicker(
          type: DateTimePickerType.date,
          initialValue: '',
          icon: const Icon(Icons.event),
          firstDate: DateTime(1900),
          lastDate: DateTime(2023),
          dateLabelText: 'Date',
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Please select a date';
            }
          }),
    );
    // child: TextFormField(
    //   decoration: const InputDecoration(
    //     icon: Icon(Icons.person),
    //     labelText: "Date of Birth",
    //   ),
    //   keyboardType: TextInputType.datetime,
    //   // validator: _emailValidator,
    // ));
  }

  Widget addressField() {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: TextFormField(
            decoration: const InputDecoration(
              icon: Icon(Icons.home),
              labelText: "Address",
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter your address';
              }
              return null;
            }));
  }

  Widget loginButton() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: TextButton(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
            foregroundColor: MaterialStateProperty.all<Color>(Colors.white)),
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('Processing Data')),
            );
          }
        },
        child: const Text("Login"),
      ),
    );
  }

  // String _emailValidator(String? value) {
  //   return "Empty";
  // }

}
