import 'package:flutter/material.dart';
import '../validation/mixin_common_validation.dart';
import '../bloc/bloc_stream.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login me',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Login'),
        ),
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<StatefulWidget> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;

  final bloc = Bloc();
  String errorMessage = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            Container(
              margin: EdgeInsets.only(top: 20.0),
            ),
            passwordField(),
            Container(
              margin: EdgeInsets.only(top: 40.0),
            ),
            loginButton()
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: 'Email address',
        errorText: errorMessage,
      ),
      // validator: validateEmail,

      onSaved: (value) {
        email = value as String;
      },
      onChanged: (String value) {
        print('onChanged email: $value');
        // bloc.getEmailStreamController().sink.add(value);
        // bloc.emailStreamController.sink.add(value);
        bloc.changeEmail(value);

        if (!value.contains('@')) {
          errorMessage = '$value is an invalid email.';
        } else {
          errorMessage = '';
        }
        setState(() {
          print('Update the state of the screen...');
        });

        // Use stream to capture data....
      },
    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration:
          InputDecoration(icon: Icon(Icons.password), labelText: 'Password'),
      validator: validatePassword,
      onSaved: (value) {
        password = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();

            print('email=$email');
            print('Demo only: password=$password');
          }
        },
        child: Text('Login'));
  }
}
