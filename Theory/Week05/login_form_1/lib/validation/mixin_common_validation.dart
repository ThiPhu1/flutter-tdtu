mixin CommonValidation {

  String? validateEmail(String? value) {
    if (!value!.contains('@')) {
      return "Pls input valid email.";
    }

    return null;
  }

  String? validatePassword(String? value) {
    if (value!.length < 5) {
      return "Password has at least 5 characters.";
    }

    return null;
  }
}