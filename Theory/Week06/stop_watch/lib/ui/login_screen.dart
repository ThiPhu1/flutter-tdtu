import 'package:flutter/material.dart';

import '../validators/loginValidators.dart';

import './stopwatch_screen.dart';

class LoginScreen extends StatefulWidget {
  static const route = '/login';

  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<StatefulWidget> with LoginValidator {
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: loginForm(),
    );
  }

  loginForm() {
    return Form(
        key: formKey,
        child: Column(
          children: [emailField(), passwordField(), loginButton()],
        ));
  }

  emailField() {
    return TextFormField(
      controller: emailController,
      decoration: const InputDecoration(
          icon: Icon(Icons.person), labelText: 'Email address'),
      validator: validateEmail,
    );
  }

  passwordField() {
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      decoration: const InputDecoration(
          icon: Icon(Icons.password), labelText: 'Password'),
      validator: validatePassword,
    );
  }

  loginButton() {
    return ElevatedButton(onPressed: validate, child: Text('Login'));
  }

  void validate() {
    final form = formKey.currentState;
    if (!form!.validate()) {
      return;
    } else {
      final email = emailController.text;
      final password = passwordController.text;

      // Navigate to stopwatch screen on validation complete
      Navigator.of(context)
          .pushReplacementNamed(StopwatchScreen.route, arguments: email);
    }
  }
}
