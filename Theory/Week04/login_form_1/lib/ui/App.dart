import 'package:flutter/material.dart';
import 'package:date_time_picker/date_time_picker.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(title: Text("My login form")),
      body: LoginForm(),
    ));
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<LoginForm> {
  // Define global key for form
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            emailField(),
            passwordField(),
            loginButton()
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: TextFormField(
            decoration: const InputDecoration(
              icon: Icon(Icons.person),
              labelText: "Email Address",
            ),
            keyboardType: TextInputType.emailAddress,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please provide an email address';
              } else if (!value.contains("@")) {
                return 'Email not valid';
              }
              return null;
            }));
  }

  Widget passwordField() {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: TextFormField(
          decoration: const InputDecoration(
            icon: Icon(Icons.lock),
            labelText: "Password",
          ),
          keyboardType: TextInputType.visiblePassword,
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Please enter password';
            } else
            if(!(value.length < 8)){
                return 'Min length is 8!';
            }
            for (var i = 0; i < value.length; i++) {
              if (isUpercase(value[i])) {
                return 'Password must has at least 1 Uppercase character!';
              }
            } 
            if (!value.contains(new RegExp(r'[a-z]'))) {
              return 'Password must has at least 1 normal character!';
            }
            if (!value.contains(new RegExp(r'^[a-zA-Z0-9_\-=@,\.;]+$'))) {
              return 'Password must has at least 1 special character!';
            }
          },
        ));
  }

  bool isUpercase(String value) {
    if (value.toUpperCase() != value) {
      return false;
    }
    return true;
  }

  Widget loginButton() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: TextButton(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
            foregroundColor: MaterialStateProperty.all<Color>(Colors.white)),
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('Processing Data')),
            );
          }
        },
        child: const Text("Login"),
      ),
    );
  }

  // String _emailValidator(String? value) {
  //   return "Empty";
  // }

}
