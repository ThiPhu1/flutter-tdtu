import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(title: Text("My login form")),
      body: LoginForm(),
    ));
  }
}

class LoginForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<LoginForm> {
  // Define global key for form
  final _formKey = GlobalKey<FormState>();

  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [emailField(), passwordField(), loginButton()],
        ),
      ),
    );
  }

  Widget emailField() {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 16),
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: TextFormField(
          decoration: InputDecoration(
            icon: Icon(Icons.person),
            labelText: "Name",
          ),
          keyboardType: TextInputType.emailAddress,
          validator: _emailValidator,
        ));
  }

  Widget passwordField() {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 16),
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: TextFormField(
          decoration: InputDecoration(
            icon: Icon(Icons.lock),
            labelText: "Password",
          ),
          obscureText: true,
        ));
  }

  Widget loginButton() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      child: TextButton(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
            foregroundColor: MaterialStateProperty.all<Color>(Colors.white)),
        onPressed: () => {
          if(_formKey.currentState!.validate())
        },
        child: Text("Login"),
      ),
    );
  }

  String _emailValidator(String value) {
    return value == null ? "Empty" : null;
  }
}
