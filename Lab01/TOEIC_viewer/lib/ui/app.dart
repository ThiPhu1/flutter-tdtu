import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../model/Question.dart';

class CardWidget extends StatefulWidget {
  @override
  _CardWidget createState() => _CardWidget();
}

class _CardWidget extends State<CardWidget> {
  List<QuestionModel> questions = [];

  final baseUrl = 'https://thachln.github.io/toeic-data/ets/2016/1/p1/';

  fetchData() async {
    var url = Uri.parse(
        'https://thachln.github.io/toeic-data/ets/2016/1/p1/data.json');
    var response = await http.get(url);

    var jsonObject = json.decode(response.body);
    for (var i = 0; i < jsonObject.length; i++) {
      var questionModel = QuestionModel(
          jsonObject[i]["no"], jsonObject[i]["image"], jsonObject[i]["audio"]);
      questions.add(questionModel);
    }

    setState(() {});

    // print(response.body);
    print(questions);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("TOEIC Viewer 0.0.1")),
        body: ListView.builder(
          itemCount: questions.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 16),
              padding: EdgeInsets.all(16),
              // decoration: BoxDecoration(
              //   borderRadius: BorderRadius.circular(13),
              // ),
              child: Card(
                child: Column(
                  children: [
                    Text(questions[index].no.toString()),
                    Container(
                      padding: EdgeInsets.all(16),
                      child: Image.network(baseUrl + questions[index].image),
                    )
                  ],
                ),
              ),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: fetchData,
        ),
      ),
    );
  }
}
