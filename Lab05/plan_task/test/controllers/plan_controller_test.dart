import 'package:flutter_test/flutter_test.dart';
import '../controllers/plan_controller_test.dart';

void main() {
  test('Test addNewPlan', () {
    var controller = PlanController();
    controller.addNewPlan('A');

    expect(1, controller.plans.length);
    expect('A', controller.plans.first.name);
  });

  test('Test addNewPlan with Vietnamese name', () {
    var controller = PlanController();
    controller.addNewPlan('Clean dishes.');

    expect(1, controller.plans.length);
    expect('Clean dishes.', controller.plans.first.name);
  });
}
